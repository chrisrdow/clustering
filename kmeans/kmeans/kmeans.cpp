// kmeans.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <fstream>      // std::ifstream
#include <vector>
#include <random>
#include <time.h>

using namespace cv;
using namespace std;

struct mem
{
	int member;
	int center;

};

bool compareByCenter(const mem &a, const mem &b)
{
	return a.center < b.center;
}

void computeMemberShip(double *X, int nobs, double * C, int ncenters, int nvars, vector<mem> & membership) {

	double * dists = new double[nobs];
	double sum_2;
	
	membership.clear();
	for (int n = 0; n < nobs; n++) 
	{

		double smallest = 1000000.0;
		int sindex = 0;

		for (int j = 0; j < ncenters; j++) {

			sum_2 = 0;
			for (int i = 0; i < nvars; i++) {
				sum_2 += (X[n*nvars + i] - C[j*nvars + i])*(X[n*nvars + i] - C[j*nvars + i]);
			}

			sum_2 = sqrt(sum_2);
			dists[j] = sum_2;
		}

		for (int mm = 0; mm < ncenters; mm++)
			if (dists[mm] < smallest) {

				smallest = dists[mm];
				sindex = mm;
			}

		mem m0;
		m0.center = sindex;
		m0.member = n;
		membership.push_back(m0);

	}
	sort(membership.begin(), membership.end(), compareByCenter);
	return;
}

void averageCenter(double *X, int nobs, double * C, int &ncenters, int nvars, vector<mem> & membership) {
	
	int k = 0;
	double * av = new double[nvars];
	
	int c = 0;
	int nm = 0;
	int memb = -1;
	int missingCent = 0;
	int ncts = 0;
	int ii = 0;

	for (int i = 0; i < ncenters; i++) {

		nm = 0;

		for (int jj = 0; jj < nvars; jj++) {
			av[jj] = 0.0;
		}

		while(membership.at(ii).center == c)
		{
			for (int j = 0; j < nvars; j++) {
				av[j] += X[membership.at(ii).member*nvars + j];
			}

			if (membership.size() - 1 == ii)
				break;

			nm++;
			ii++;

		}
		if(nm != 0)
		{
			
				for (int jj = 0; jj < nvars; jj++) {
					C[(membership.at(i).center)*nvars + jj] = av[jj] / (double)nm;
	
				}							
				ncts++;
			
		}
		else {
			missingCent++;
		}
		c++;

		if (membership.size() - 1 == ii)
			break;
	}
//	ofstream ofsm;
//	ofsm.seekp(0);
//	ofsm.open("member.txt", ios::app);
//	if (ofsm.is_open()) {
//		for (int i = 0; i < membership.size(); i++) {
//
//			ofsm << membership.at(i).member;
//			ofsm << "  member of :";
//			ofsm << membership.at(i).center;
//
//			ofsm << endl;
//		}
//	}
//	ofsm.close();	//ncenters = ncenters - missingCent;
	return;
}

int cluster(double *X, int nobs, double * C, int ncenters, int nvars, int iterations) {
		 
	vector<mem> membership;
	/*
	ofstream ofs;
	ofs.open("r_centers.txt", ios::app);
	if (ofs.is_open())
	{
		for (int i = 0; i < ncenters; i++) {
			for (int j = 0; j < nvars; j++)
			{
				if (ofs.is_open()) {
					ofs << C[i*nvars + j];
					ofs << ",";
				}
				
			}

			ofs << endl;
		}
	}
	ofs.close();
	*/

	for (int n = 0; n < iterations; n++) {
		computeMemberShip(X, nobs, C, ncenters, nvars, membership);
		/*
		ofstream ofsm;
		ofsm.open("member.txt", ios::app);
		if (ofs.is_open()) {
			for (int i = 0; i < membership.size(); i++) {

				ofsm << membership.at(i).member;
				ofsm << "  member of :";
				ofsm << membership.at(i).center;

				ofsm << endl;
			}
		}
		ofsm.close();
		*/

		averageCenter(X, nobs, C, ncenters, nvars, membership);
	}
	/*	
	ofstream ofsc;
	ofsc.open("c_centers.txt", ios::app);
	if (ofsc.is_open())
	{
		for (int i = 0; i < ncenters; i++) {
			for (int j = 0; j < nvars; j++)
			{

				ofsc << C[(i)*nvars + j];
				ofsc << ",";
				
				
			}
			ofsc << endl;
		}
	}
	ofsc.close();
	*/
	return 0;
}



int sampleCenters(double *X, int nobs, double *C, int ncenters, int nvars) {

	vector<int> obs;
	 
	for(int i = 0; i < nobs; i ++) obs.push_back(i);

	srand((unsigned int)time(0));
	random_shuffle(obs.begin(), obs.end());


	for (int j = 0; j < ncenters; j++) {
		for (int i = 0; i < nvars; i++) {
			C[j*nvars + i] = X[obs.at(j)*nvars + i];
		}

	}
//	for (int i = 0; i < 20; i++) {
//		cout << obs.at(i);
//		cout << endl;
//	}
	return 0;
}

int writeCentersToCsv(string filename,double * C,int ncenters,int nvars) {
	
	ofstream ofs;
	stringstream convert;
	string row;

	ofs.open(filename, ios::app);
	if (ofs.is_open())
	{
		ofs << nvars;
		ofs << endl;
		ofs << ncenters;
		ofs << endl;
		for (int j = 0; j < ncenters; j++) {

			for (int i = 0; i < nvars; i++) {

				convert << C[j*nvars +i];
				convert << ",";

			}

			row = convert.str();
			convert.clear();//clear any bits set
			convert.str(string());
			ofs << row;
			ofs << std::endl;
		}


	}
	ofs.close();

	return 0;
}
int readcsvIntoMat(string file,double*& X, int &nobs, int &nvars) {

	ifstream ifcsv;
	ifstream ifs;
	string line;
	stringstream convert;
	string token;

	ifs.open(file.c_str(), ifstream::in);
	if (ifs.is_open())
	{
		getline(ifs, line); // 
		getline(ifs, line); //
		while (ifs.good())
		{
			getline(ifs, line);
			nobs++;
		}

	}
	ifs.close();

	ifs.open(file.c_str(), ifstream::in);
	if (ifs.is_open())
	{
		getline(ifs, line);
		getline(ifs, line);
		getline(ifs, line);
		string delimiter = ",";
		size_t pos = 0;

		while ((pos = line.find(delimiter)) != string::npos) {

			token = line.substr(0, pos);
			line.erase(0, pos + delimiter.length());

			nvars++;
			
		}
		
	}
	ifs.close();


	X = new double[nobs*nvars];

	ifs.open(file.c_str(), ifstream::in);
	if (ifs.is_open())
	{
		int jj = 0;
		getline(ifs, line);
		getline(ifs, line);
		while (ifs.good())
		{
			getline(ifs, line);
			string delimiter = ",";
			size_t pos = 0;
			double value = 0.0;
			
			int ii = 0;
			while ((pos = line.find(delimiter)) != string::npos) {

				token = line.substr(0, pos);
				line.erase(0, pos + delimiter.length());
				stringstream convert(token);

				convert >> X[jj*nvars + ii];
				ii++;

			}
			jj++;
		}
	}
	ifs.close();

	return 0;
}



int main()
{
	double *X = NULL;
	double *C = NULL;
	string file = "hist0.csv";
	string cfile = "chist0.csv";

	int nobs = 0;
	int nvars = 0;
	int its = 100;
	int ncenters = 100;
	
//	vector<mem> meber;
//	mem mshp;
//	sort(meber.begin(), meber.end(), compareByCenter);
//	for (int i = 0; i < meber.size(); i++) {
//		cout << meber.at(i).center;
//		cout << endl;
//	}

	readcsvIntoMat(file, X, nobs, nvars);
	C = new double[ncenters*nvars];

	sampleCenters(X,nobs, C, ncenters, nvars);
//	cluster(X, nobs, C, ncenters, nvars, its);
	writeCentersToCsv(cfile, C, ncenters, nvars);

    return 0;
}

